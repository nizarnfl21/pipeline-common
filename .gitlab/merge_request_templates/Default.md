## What does this MR do?

<!-- Briefly describe what this MR is about and why the change is needed -->

## Check-list

- [ ] Verify the change in [all affected pipelines](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures/#scheduled-qa-test-pipelines).

/label ~"type::maintenance" ~"maintenance::pipelines"

<!-- template sourced from https://gitlab.com/gitlab-org/quality/pipeline-common/-/blob/master/.gitlab/merge_request_templates/Default.md -->
